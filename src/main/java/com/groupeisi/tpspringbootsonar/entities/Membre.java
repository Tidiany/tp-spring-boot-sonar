package com.groupeisi.tpspringbootsonar.entities;


import jakarta.persistence.*;
import lombok.*;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.UUID;


/**
 * @author Tidiany
 *
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(
        name = Membre.TABLE_NAME,
        indexes = {
            @Index(name = "nom_index", columnList = "nom"),
            @Index(name = "prenom_index", columnList = "prenom")
        }
)
public class Membre implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;

    public static final String TABLE_NAME = "membres";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(unique = true)
    private String code;
    private String nom;
    private String prenom;
    @Column(name = "num_tel_1")
    private String numTel1;
    @Column(name = "num_tel_2")
    private String numTel2;
    private String residence;
    @Column(name = "date_inscription")
    private LocalDate dateInscription;

    /*
        Meta data... Must be completed
        @Todo Create Meta Entity for the externalisation of all metadata.
    */
    @Column(name = "external_id", unique = true)
    private UUID externalId;

    @Column(name = "created_at")
    private LocalDate createdAt;

    @Column(name = "last_updated_at")
    private LocalDate lastUpdatedAt;
}
