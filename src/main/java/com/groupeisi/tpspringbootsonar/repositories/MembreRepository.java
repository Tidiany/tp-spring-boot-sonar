package com.groupeisi.tpspringbootsonar.repositories;

import com.groupeisi.tpspringbootsonar.entities.Membre;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

/**
 * @author Tidiany
 *
 */
@Repository
public interface MembreRepository extends JpaRepository<Membre, Long> {
    Optional<Membre> findByExternalId(UUID exernalId);
}
