package com.groupeisi.tpspringbootsonar.services;

import java.util.List;

/**
 * @author Tidiany
 * Une interface générique permettant de fair un CRUD
 * en prenant en paramettre le DTO.
 */
public interface CrudService<D> {
    List<D> getAll();
    D getById(Long id);
    D save(D dto);
    D update(D dto);
    void delete(D dto);
}
