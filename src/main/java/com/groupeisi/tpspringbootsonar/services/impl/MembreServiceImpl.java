package com.groupeisi.tpspringbootsonar.services.impl;

import com.groupeisi.tpspringbootsonar.entities.Membre;
import com.groupeisi.tpspringbootsonar.repositories.MembreRepository;
import com.groupeisi.tpspringbootsonar.services.MembreService;
import com.groupeisi.tpspringbootsonar.web.dtos.MembreDTO;
import com.groupeisi.tpspringbootsonar.web.mapper.MembreMapper;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

/**
 * @author Tidiany
 *
 */
@Service
public class MembreServiceImpl implements MembreService {
    private static MembreRepository membreRepository;

    @Override
    public List<MembreDTO> getAll() {
        return membreRepository.findAll().stream().map(MembreMapper.INSTANCE::entityToDto).toList();
    }

    @Override
    public MembreDTO getById(Long id) {
        Optional<Membre> optionalMembre = membreRepository.findById(id);
        return optionalMembre.map(MembreMapper.INSTANCE::entityToDto).orElse(null);
    }

    @Override
    public MembreDTO save(MembreDTO dto) {
        Membre membre = MembreMapper.INSTANCE.dtoToEntity(dto);
        membre.setExternalId(UUID.randomUUID());
        membre.setCreatedAt(LocalDate.now());
        return MembreMapper.INSTANCE.entityToDto(membreRepository.save(membre));
    }

    @Override
    public MembreDTO update(MembreDTO dto) {
        Optional<Membre> optionalMembre = membreRepository.findByExternalId(dto.extenalId());
        if (optionalMembre.isPresent()) {
            Membre membre = optionalMembre.get();
            membre = Membre.builder()
                    .id(membre.getId())
                    .code(membre.getCode())
                    .nom(membre.getNom())
                    .prenom(membre.getPrenom())
                    .dateInscription(membre.getDateInscription())
                    .lastUpdatedAt(LocalDate.now())
                    .numTel1(membre.getNumTel1())
                    .numTel2(membre.getNumTel2())
                    .externalId(membre.getExternalId())
                    .residence(membre.getResidence())
                    .build();
            return MembreMapper.INSTANCE.entityToDto(membreRepository.save(membre));
        }
        return null;
    }

    @Override
    public void delete(MembreDTO dto) {
        membreRepository.delete(MembreMapper.INSTANCE.dtoToEntity(dto));
    }
}
