package com.groupeisi.tpspringbootsonar.services;

import com.groupeisi.tpspringbootsonar.web.dtos.MembreDTO;

/**
 * @author Tidiany
 *
 */
public interface MembreService extends CrudService<MembreDTO> {

}
