package com.groupeisi.tpspringbootsonar;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TpSpringBootSonarApplication {

    public static void main(String[] args) {
        SpringApplication.run(TpSpringBootSonarApplication.class, args);
    }

    public String sayHello(){
        return "Hello";
    }
}
