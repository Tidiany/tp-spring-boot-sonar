package com.groupeisi.tpspringbootsonar.web.mapper;

public interface MapperFactory<E, D> {
    D entityToDto(E entity);
    E dtoToEntity(D dto);
}
