package com.groupeisi.tpspringbootsonar.web.mapper;

import com.groupeisi.tpspringbootsonar.entities.Membre;
import com.groupeisi.tpspringbootsonar.web.dtos.MembreDTO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface MembreMapper extends MapperFactory<Membre, MembreDTO> {
    MembreMapper INSTANCE = Mappers.getMapper(MembreMapper.class);
}
