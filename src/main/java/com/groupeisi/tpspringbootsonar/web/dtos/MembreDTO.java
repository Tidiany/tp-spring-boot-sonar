package com.groupeisi.tpspringbootsonar.web.dtos;

import java.time.LocalDate;
import java.util.UUID;

/**
 * @author Tidiany
 *
 */
public record MembreDTO(
        UUID extenalId,
        String code,
        String nom,
        String prenom,
        String numTel1,
        String numTel2,
        String residence,
        LocalDate dateInscription
) {
}
