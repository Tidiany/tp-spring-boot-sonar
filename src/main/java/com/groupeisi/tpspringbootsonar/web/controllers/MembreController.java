package com.groupeisi.tpspringbootsonar.web.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Tidiany
 *
 */
@RestController
@RequestMapping("/membre")
public class MembreController {

    @GetMapping("/{name}")
    public String welcome(@PathVariable(required = false) String name){
        name = name == null ? "Word" : name;
        return """
                Hello %s,
                Welcome to our site.
                
                Cordialement,
                Tidiany.
                """.formatted(name);
    }
}
