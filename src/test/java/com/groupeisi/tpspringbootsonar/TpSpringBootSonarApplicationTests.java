package com.groupeisi.tpspringbootsonar;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class TpSpringBootSonarApplicationTests {

    private TpSpringBootSonarApplication tpSpringBootSonarApplication;

    @BeforeEach
    public void init(){
        tpSpringBootSonarApplication = new TpSpringBootSonarApplication();
    }

    @Test
    void contextLoads() {
        Assertions.assertEquals("Hello", tpSpringBootSonarApplication.sayHello());
    }

}
